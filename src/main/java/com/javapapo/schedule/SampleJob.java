package com.javapapo.schedule;

import io.micronaut.scheduling.annotation.Scheduled;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.micronaut.SchedulerLock;

import javax.inject.Singleton;

@Singleton
@Slf4j
public class SampleJob {

    @Scheduled(fixedDelay = "20s", initialDelay = "5s")
    @SchedulerLock(name = "redislockjob",lockAtLeastFor = "5s")
    public void execute(){
        log.info("Job executed!");
    }

}
