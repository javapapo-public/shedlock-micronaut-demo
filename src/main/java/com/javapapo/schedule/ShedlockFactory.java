package com.javapapo.schedule;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Value;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.redis.jedis.JedisLockProvider;
import redis.clients.jedis.JedisPool;

@Factory
public class ShedlockFactory {

    @Bean
    public JedisPool jedisPool(@Value("${redis.host}") String host, @Value("${redis.port}") int port){
        return new JedisPool(host,port);
    }

    @Bean
    public LockProvider lockProvider(JedisPool jedisPool) {
        return new JedisLockProvider(jedisPool);
    }

}
