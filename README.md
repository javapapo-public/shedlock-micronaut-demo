## Micronaut Shedlock example

Repo for [this]() blog spot.

## How to test
You need to have docker running on your machine. Then execute the following script. It will use `docker-compose` to spin 
2 instances of a simple Micronaut Service and Redis. 

```bash
./run.sh
```